import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  ip = 'localhost';
  constructor(private readonly http: HttpClient) { }

  getRecipeList(): Promise<any> {
    return this.http.get(`http://${this.ip}:8082/api/recette`).toPromise();
  }

  getPdf(recipeIdList: number[]): Promise<any> {
    return this.http.post(`http://${this.ip}:8082/api/pdf/allById`, recipeIdList, {
      'responseType': 'arraybuffer' as 'json'
    }).toPromise();
  }
}
