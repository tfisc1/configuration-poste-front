import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {

  @Input() recipes: { id: number, libelle: string, ingredient: string }[] = [];
  @Input() title: string;
  @Input() actionText: string;
  @Input() listColor: string;
  @Output() selectedRecipe: EventEmitter<{ id: number, libelle: string, ingredient: string }> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  recipeClicked(recipe: { id: number, libelle: string, ingredient: string }) {
    this.selectedRecipe.emit(recipe);
  }

}
