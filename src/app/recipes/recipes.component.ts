import { Component, OnInit } from '@angular/core';
import { RecipesService } from './recipes.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements OnInit {

  recipes: { id: number, libelle: string, ingredient: string }[] = [];

  selectedRecipes: { id: number, libelle: string, ingredient: string }[] = [];

  constructor(private readonly recipesService: RecipesService) { }

  async ngOnInit() {
    this.recipes = await this.recipesService.getRecipeList();
  }

  addRecipeToBook(recipe: { id: number, libelle: string, ingredient: string }) {
    this.recipes = this.recipes.filter(rec => rec.id !== recipe.id);
    this.selectedRecipes.push(recipe);
  }

  removeRecipeFromBook(recipe: { id: number, libelle: string, ingredient: string }) {
    this.recipes.push(recipe);
    this.selectedRecipes = this.selectedRecipes.filter(rec => rec.id !== recipe.id);
  }

  async downloadBook() {
    const recipesIdList: number[] = this.selectedRecipes.map(recipe => recipe.id);
    const result = await this.recipesService.getPdf(recipesIdList);
    const blob = new Blob([result], { type: 'application/pdf' });
    saveAs(blob, 'recettes');
  }

}
